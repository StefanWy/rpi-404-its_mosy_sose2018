package haw_hamburg.rpi_404;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import processing.android.PFragment;

//================================================================================================//
// Projekt: RPi-404         Author: Pieter Haase                                                  //
//------------------------------------------------------------------------------------------------//
// Main-Funktion der App; sie initialisiert und verwaltet sämtliche PApletts und den MQTT Service //
//================================================================================================//

public class MainActivity extends AppCompatActivity {
    private Radar radar;
    private Radar radarLarge;
    private Joystick joystick;
    private MQTTService mqttService;
    private CameraAngleBar cameraAngleBar;
    private MotionSensor sensorTest;
    private Separator separator;


    private float[] radarArray = new float[270];        // Gemeinsames Array für die beiden Radars, in dem für sämtliche Winkel die gemessene Distanz gespeichert wird

    // vorherige X/Y Werte für Swipe-Geste
    private float previousTouchX1 = 0;
    private float previousTouchY1 = 0;
    private float previousTouchX2 = 0;
    private float previousTouchY2 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);   // Fullscreen ohne TitleBar
        setContentView(R.layout.activity_main);
        findViewById(R.id.main_layout).setBackgroundColor(Color.parseColor(Settings.backgroundColor));

        // Separator initialisieren und einem View zuweisen
        separator = new Separator();
        View separatorView = findViewById(R.id.separator);
        PFragment separatorFragment = new PFragment(separator);
        separatorFragment.setView(separatorView,this);

        // Kleines Radar initialisieren und einem View zuweisen
        radar = new Radar(this, Radar.SMALL);
        View radarView = findViewById(R.id.radar);
        PFragment radarFragment = new PFragment(radar);
        radarFragment.setView(radarView, this);
        radar.resume();                                                 // Radar starten

        // Großes Radar initialisieren und einem View zuweisen
        radarLarge = new Radar(this, Radar.LARGE);
        View radarLargeView = findViewById(R.id.someName);
        PFragment radarLargeFragment = new PFragment(radarLarge);
        radarLargeFragment.setView(radarLargeView, this);

        // MQTT initialisieren
        mqttService = new MQTTService(this);

        // Slider für die Kamerabewegung initialisieren und einem View zuweisen
        cameraAngleBar = new CameraAngleBar(this);
        View cameraAngleView = findViewById(R.id.cameraAngleBar);
        PFragment cameraAngleFragment = new PFragment(cameraAngleBar);
        cameraAngleFragment.setView(cameraAngleView,this);
        cameraAngleBar.setMqttService(mqttService);

        // Joystick initialisieren und einem View zuweisen
        joystick = new Joystick(mqttService);
        View joystickView = findViewById(R.id.joystick);
        PFragment joystickFragment = new PFragment(joystick);
        joystickFragment.setView(joystickView, this);

        // Hintergrundfarbe des Camera-Feeds setzen
        View videoFrame = findViewById(R.id.videostream);
        videoFrame.setBackgroundColor(Color.parseColor(Settings.backgroundColor));

        // Kleines Fenster für Camera-Feed initialisieren und einem View zuweisen
        WebView webView = findViewById(R.id.webViewLarge);
        VideoView videoView = new VideoView(this, webView);

        // Großes Fenster für Camera-Feed initialisieren und einem View zuweisen
        WebView webView2 = findViewById(R.id.webViewSmall);
        VideoView videoView2 = new VideoView(this, webView2);
        webView2.setVisibility(View.INVISIBLE);                                 // erst einmal unsichtbar

        // Motion Sensor Steuerung initialisieren
        sensorTest = new MotionSensor(this, cameraAngleBar);

        // Button Overlay des Camera-Feeds für Swipe-Geste konfigurieren
        Button button = findViewById(R.id.cameraButtonOverlay);
        button.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:                   // Bei Berührung X/Y-Werte merken
                        previousTouchX1 = event.getX();
                        previousTouchY1 = event.getY();
                        sensorTest.onButtonPressed();               // Aktivieren der Bewegungssensorsteuerung für die Kamera
                        return true;
                    case MotionEvent.ACTION_UP:
                        if(event.getX() < previousTouchX1 && event.getY() < previousTouchY1){       // Wurde nach oben-links geswiped
                            if (webView.getVisibility() == View.VISIBLE){                           // Sichtbarkeit des Camera-Feeds togglen
                                webView2.setVisibility(View.VISIBLE);
                                webView.setVisibility(View.INVISIBLE);
                            } else {
                                webView2.setVisibility(View.INVISIBLE);
                                webView.setVisibility(View.VISIBLE);
                            }
                            if(!Settings.testMode)
                                radar.resume();
                            radarLarge.pause();

                        }
                        sensorTest.onButtonReleased();
                        return true;
                }
                return false;
            }
        });

        // Button Overlay des kleinen Radars für Swipe-Geste konfigurieren
        Button button2 = findViewById(R.id.radarButtonOverlay);
        button2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:                 // Bei Berührung X/Y-Werte merken
                        previousTouchX2 = event.getX();
                        previousTouchY2 = event.getY();
                        return true;
                    case MotionEvent.ACTION_UP:
                        if(event.getX() > previousTouchX2 && event.getY() > previousTouchY2){       // Wurde nach oben-links geswiped
                            if (webView.getVisibility() == View.VISIBLE){                           // Sichtbarkeit des Camera-Feeds togglen
                                webView2.setVisibility(View.VISIBLE);
                                webView.setVisibility(View.INVISIBLE);
                            } else {
                                webView2.setVisibility(View.INVISIBLE);
                                webView.setVisibility(View.VISIBLE);
                            }
                            if(!Settings.testMode)
                                radar.pause();
                            radarLarge.resume();
                        }
                        return true;
                }
                return false;
            }
        });

        // Kalibrierungs-Button konfigurieren
        Button button3 = findViewById(R.id.calibrateButton);
        button3.setTextColor(Color.parseColor(Settings.mainColor));
        button3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        button3.setTextColor(Color.parseColor(Settings.accent3Color));
                        mqttService.sendCalibrate();
                        Toast.makeText(MainActivity.this, "Calibrating...", Toast.LENGTH_SHORT).show();
                        return true;
                    case MotionEvent.ACTION_UP:
                        button3.setTextColor(Color.parseColor(Settings.mainColor));
                        return true;
                }
                return false;
            }
        });
    }

    public Radar getRadar() {
        return radar;
    }

    public void setRadarArray(float[] array){
        radarArray = array;
    }

    // Gemeinsames Winkel-Distanz Array für die beiden Radars Updaten
    public void updateArray(int angle, float distance){
        if (distance > 53)
            distance = 53;
        if (angle < 270){
            radar.updateArray(angle, distance);
            radarLarge.updateArray(angle, distance);
            radarArray[angle] = distance;
        }
    }

    public CameraAngleBar getCameraAngleBar() {
        return cameraAngleBar;
    }

    // Sensor-Listener bei Schließen der App stoppen
    public void onDestroy(){
        super.onDestroy();
        sensorTest.unregisterListeners();
    }
}
